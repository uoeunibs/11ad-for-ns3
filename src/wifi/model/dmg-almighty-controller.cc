/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015 NICOLO' FACCHI
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Nicolo' Facchi <nicolo.facchi@gmail.com>
 */


#include "dmg-almighty-controller.h"
#include "ns3/log.h"
#include "ns3/internet-module.h"
#include "ns3/wifi-net-device.h"
#include "ns3/mobility-model.h"
#include "dmg-destination-fixed-wifi-manager.h"
#include "dmg-wifi-mac.h"
#include "yans-wifi-phy.h"
#include "yans-wifi-channel.h"
#include "edca-txop-n.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/pointer.h"

NS_LOG_COMPONENT_DEFINE ("DmgAlmightyController");

namespace ns3 {

DmgAlmightyController::DmgAlmightyController ()
{
  m_apNodes = 0;
  m_staNodes = 0;

  // Configure some default values. Not all these values are used in the current
  // implementation.
  m_antennaSectorsN = 4; // by default 4 sectors (90 degress)
  m_energyDetectionThreshold = -96.0;
  m_ccaMode1Threshold = -99;
  m_txGain = 1;
  m_rxGain = 1;
  m_txPowerLevels = 1;
  m_txPowerEnd = 16.0206;
  m_txPowerStart = 16.0206;
  m_rxNoiseFigure = 7;

  m_biDuration = 1024000 * 100;
  m_biOverhaedFraction = 0;
}

DmgAlmightyController::~DmgAlmightyController ()
{
  delete m_apNodes;
  m_apNodes = 0;

  delete m_staNodes;
  m_staNodes = 0;
}

void
DmgAlmightyController::SetApNodes (NodeContainer *aps)
{
  delete m_apNodes;
  m_apNodes = 0;
  m_apNodes = new NodeContainer(*aps);
}

NodeContainer *
DmgAlmightyController::GetApNodes (void)
{
  return m_apNodes;
}

void
DmgAlmightyController::SetStaNodes (NodeContainer *stas)
{
  delete m_staNodes;
  m_staNodes = 0;
  m_staNodes = new NodeContainer(*stas);
}

NodeContainer *
DmgAlmightyController::GetStaNodes (void)
{
  return m_staNodes;
}

void
DmgAlmightyController::SetAssocPairs (std::vector<uint32_t> pairs)
{
  m_assocPairs = pairs;
}

std::vector<uint32_t>
DmgAlmightyController::GetAssocPairs (void)
{
  return m_assocPairs;
}

void
DmgAlmightyController::EnforceAssociations (void)
{
  // At the moment DMG AP and STA are implemented as an extension of
  // RegularWifiMac and they do not require any form of association.
  // Association is enforced at the application level.
  for (uint32_t i = 0; i < m_assocPairs.size(); i++) {
    NS_LOG_INFO("STA " << i << " assoc to AP " << m_assocPairs.at(i));
  }
}

void
DmgAlmightyController::EnforceRouting (void)
{
  // At the moment we suppose that the application transmissions are one-hop
  // (STA to AP). This means that routing is not required
  //in future releases DmgScenario helper must take care of setting up the applications in the
  // propoer way
  for (uint32_t i = 0; i < m_assocPairs.size(); i++) {
    NS_LOG_INFO("STA " << i << " TX to AP " << m_assocPairs.at(i));
  }
}

void
DmgAlmightyController::SetAntennaSectorsN (double sectorsN)
{
  // Not used in current implementation.
  m_antennaSectorsN = sectorsN;
  // TODO: compute m_antennaBeamwidth and m_antennaGainTxRx
}

double
DmgAlmightyController::GetAntennaSectorsN (void)
{
  return m_antennaSectorsN;
}

void
DmgAlmightyController::SetAntennaBeamwidth (double beamwidth)
{
  // Not used in current implementation.
  m_antennaBeamwidth = beamwidth;
  // TODO: compute m_antennaSectorsN and m_antennaGainTxRx
}

double
DmgAlmightyController::GetAntennaBeamwidth (void)
{
  return m_antennaBeamwidth;
}

void
DmgAlmightyController::SetAntennaGainTxRx (double gain)
{
  // Not used in current implementation.
  m_antennaGainTxRx = gain;
  // TODO: compute m_antennaBeamwidth and m_antennaBeamwidth
}

double
DmgAlmightyController::GetAntennaGainTxRx (void)
{
  // Not used in current implementation.
  return m_antennaGainTxRx;
}

void
DmgAlmightyController::SetEnergyDetectionThreshold (double detTh)
{
  m_energyDetectionThreshold = detTh;
}

double
DmgAlmightyController::GetEnergyDetectionThreshold (void)
{
  // Not used in current implementation.
  return m_energyDetectionThreshold;
}

void
DmgAlmightyController::SetCcaMode1Threshold (double cca)
{
  // Not used in current implementation.
  m_ccaMode1Threshold = cca;
}

double
DmgAlmightyController::GetCcaMode1Threshold (void)
{
  return m_ccaMode1Threshold;
}

void
DmgAlmightyController::SetTxGain (double gain)
{
  // Not used in current implementation.
  m_txGain = gain;
}

double
DmgAlmightyController::GetTxGain (void)
{
  return m_txGain;
}

void
DmgAlmightyController::SetRxGain (double gain)
{
  // Not used in current implementation.
  m_rxGain = gain;
}

double
DmgAlmightyController::GetRxGain (void)
{
  return m_rxGain;
}

void
DmgAlmightyController::SetTxPowerLevels (double txLevels)
{
  // Not used in current implementation.
  m_txPowerLevels = txLevels;
}

double
DmgAlmightyController::GetTxPowerLevels (void)
{
  return m_txPowerLevels;
}

void
DmgAlmightyController::SetTxPowerEnd (double txPowerEnd)
{
  // Not used in current implementation.
  m_txPowerEnd = txPowerEnd;
}

double
DmgAlmightyController::GetTxPowerEnd (void)
{
  return m_txPowerEnd;
}

void
DmgAlmightyController::SetTxPowerStart (double txPowerStart)
{
  // Not used in current implementation.
  m_txPowerStart = txPowerStart;
}

double
DmgAlmightyController::GetTxPowerStart (void)
{
  return m_txPowerStart;
}

void
DmgAlmightyController::SetRxNoiseFigure (double noiseFigure)
{
  // Not used in current implementation.
  m_rxNoiseFigure = noiseFigure;
}

double
DmgAlmightyController::GetRxNoiseFigure (void)
{
  return m_rxNoiseFigure;
}

void
DmgAlmightyController::SetBiDuration (uint64_t biDur)
{
  m_biDuration = biDur;
}

uint64_t
DmgAlmightyController::GetBiDuration (void)
{
  return m_biDuration;
}

void
DmgAlmightyController::SetBiOverheadFraction (double biOverhead)
{
  m_biOverhaedFraction = biOverhead;
}

double
DmgAlmightyController::GetBiOverheadFraction (void)
{
  return m_biOverhaedFraction;
}

void
DmgAlmightyController::SetServicePeriodAllocation (std::vector<double> spAlloc)
{
  m_spAlloc = spAlloc;
}

std::vector<double>
DmgAlmightyController::GetServicePeriodAllocation (void)
{
  return m_spAlloc;
}

void
DmgAlmightyController::ConfigureBeaconIntervals (void)
{
  uint64_t overheadDurNs = (uint64_t) ceil(m_biDuration *
					   m_biOverhaedFraction);
  uint64_t biAvailableTimeNs = m_biDuration - overheadDurNs;
  uint64_t spStartNs = overheadDurNs;

  // Set beacon interval duration on all nodes
  for (uint32_t i = 0; i < m_apNodes->GetN(); i++) {
    m_apNodes->Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetMac()->GetObject<DmgWifiMac>()->
	    GetDmgBeaconInterval()->SetBiDuration(NanoSeconds(m_biDuration));
  }

  for (uint32_t i = 0; i < m_staNodes->GetN(); i++) {
    m_staNodes->Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetMac()->GetObject<DmgWifiMac>()->
	    GetDmgBeaconInterval()->SetBiDuration(NanoSeconds(m_biDuration));
  }

  // Configure the beacon interval on all nodes
  // Each BI contains a list of 1 or more Service Periods
  for (uint32_t apIdx = 0; apIdx < m_apNodes->GetN(); apIdx++) {
    uint64_t nextSpStartNs = spStartNs;
    Ptr<DmgBeaconInterval> dmgBiAp = m_apNodes->Get(apIdx)->GetDevice(0)->
	      GetObject<WifiNetDevice>()->GetMac()->GetObject<DmgWifiMac>()->
	      GetDmgBeaconInterval();

    for (uint32_t staIdx = 0; staIdx < m_staNodes->GetN(); staIdx++) {

      if (m_assocPairs.at(staIdx) != apIdx) {
	continue;
      }

      double staFraction = m_spAlloc.at(staIdx);
      uint64_t staSpDurationNs = (uint64_t) floor(biAvailableTimeNs *
						  staFraction);
      Ptr<DmgBeaconInterval> dmgBiSta = m_staNodes->Get(staIdx)->GetDevice(0)->
	      GetObject<WifiNetDevice>()->GetMac()->GetObject<DmgWifiMac>()->
	      GetDmgBeaconInterval();

      Time spStart = NanoSeconds(nextSpStartNs);
      Time spStop = NanoSeconds(nextSpStartNs + staSpDurationNs);


      Mac48Address macAp = m_apNodes->Get(apIdx)->GetDevice(0)->
	      GetObject<WifiNetDevice>()->GetMac()->GetObject<DmgWifiMac>()->
	      GetAddress();
      Mac48Address macSta = m_staNodes->Get(staIdx)->GetDevice(0)->
	      GetObject<WifiNetDevice>()->GetMac()->GetObject<DmgWifiMac>()->
	      GetAddress();

      Ptr<MobilityModel> mobAp = m_apNodes->Get(apIdx)->
	      GetObject<MobilityModel> ();
      Ptr<MobilityModel> mobSta = m_staNodes->Get(staIdx)->
	      GetObject<MobilityModel> ();

      dmgBiAp->AddSp(spStart, spStop, macSta, mobSta);
      dmgBiSta->AddSp(spStart, spStop, macAp, mobAp);

      nextSpStartNs += staSpDurationNs;
    }
  }

  // Trigger Antenna Alignment
  // Each node will force its antenna to allign to the destination of the
  // Service Period at the beginning of that Service Period.
  for (uint32_t i = 0; i < m_apNodes->GetN(); i++) {
    m_apNodes->Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetMac()->GetObject<DmgWifiMac>()->
	    GetDmgBeaconInterval()->ScheduleNextAntennaAlignment();

    if (m_apNodes->Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->
	      GetMac()->GetObject<DmgWifiMac>()->GetDmgBeaconInterval()->
        GetSps().size() != 0) {

      m_apNodes->Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->
	      GetMac()->GetObject<DmgWifiMac>()->StartDmgSpTracking();
    }
  }

  for (uint32_t i = 0; i < m_staNodes->GetN(); i++) {
    m_staNodes->Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetMac()->GetObject<DmgWifiMac>()->
	    GetDmgBeaconInterval()->ScheduleNextAntennaAlignment();

    if (m_staNodes->Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->
	      GetMac()->GetObject<DmgWifiMac>()->GetDmgBeaconInterval()->
        GetSps().size() != 0) {

      m_staNodes->Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->
	      GetMac()->GetObject<DmgWifiMac>()->StartDmgSpTracking();
    }
  }

}

/* Utility function used internally to this module to configure the
 * WifiRemoteStationManager
 */
WifiMode
DmgAlmightyController::GetWifiMode (double rxPowerDbi, bool ofdm)
{
  std::string ret = "VHTMCS0";

  if (ofdm) {
    if (rxPowerDbi >= -47)
      ret = "VHTMCS24a";
    else if (rxPowerDbi >= -49)
      ret = "VHTMCS23a";
    else if (rxPowerDbi >= -51)
      ret = "VHTMCS22a";
    else if (rxPowerDbi >= -53)
      ret = "VHTMCS21a";
    else if (rxPowerDbi >= -54)
      ret = "VHTMCS20a";
    else if (rxPowerDbi >= -56)
      ret = "VHTMCS19a";
    else if (rxPowerDbi >= -58)
      ret = "VHTMCS18a";
    else if (rxPowerDbi >= -60)
      ret = "VHTMCS17a";
    else if (rxPowerDbi >= -62)
      ret = "VHTMCS16a";
    else if (rxPowerDbi >= -63)
      ret = "VHTMCS15a";
    else if (rxPowerDbi >= -64)
      ret = "VHTMCS14a";
    else if (rxPowerDbi >= -66)
      ret = "VHTMCS13a";
  } else {
    if (rxPowerDbi >= -53)
      ret = "VHTMCS12";
    else if (rxPowerDbi >= -54)
      ret = "VHTMCS11";
    else if (rxPowerDbi >= -55)
      ret = "VHTMCS10";
    else if (rxPowerDbi >= -59)
      ret = "VHTMCS9";
    else if (rxPowerDbi >= -61)
      ret = "VHTMCS8";
    else if (rxPowerDbi >= -62)
      ret = "VHTMCS7";
      //ret = "VHTMCS5";
    else if (rxPowerDbi >= -63)
      ret = "VHTMCS6";
    else if (rxPowerDbi >= -64)
      ret = "VHTMCS4";
    else if (rxPowerDbi >= -65)
      ret = "VHTMCS3";
    else if (rxPowerDbi >= -66)
      ret = "VHTMCS2";
    else if (rxPowerDbi >= -68)
      ret = "VHTMCS1";

  }

  return ret;
}

/* Configure, on each node, the ideal Tx MCS for each possible destination */
void
DmgAlmightyController::ConfigureWifiManager (void)
{
  for (uint32_t staIdx = 0; staIdx < m_staNodes->GetN(); staIdx++) {
    uint32_t apIdx = m_assocPairs.at(staIdx);

    Ptr<DmgWifiMac> staMac =
	    m_staNodes->Get(staIdx)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetMac()->GetObject<DmgWifiMac>();
    Ptr<DmgWifiMac> apMac =
	    m_apNodes->Get(apIdx)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetMac()->GetObject<DmgWifiMac>();

    Ptr<DmgDestinationFixedWifiManager> staManager = staMac->
	    GetWifiRemoteStationManager()->GetObject<DmgDestinationFixedWifiManager>();
    Ptr<DmgDestinationFixedWifiManager> apManager = apMac->
	    GetWifiRemoteStationManager()->GetObject<DmgDestinationFixedWifiManager>();

    Ptr<YansWifiPhy> staPhy =
	    m_staNodes->Get(staIdx)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetPhy()->GetObject<YansWifiPhy> ();
    Ptr<YansWifiPhy> apPhy =
	    m_apNodes->Get(apIdx)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetPhy()->GetObject<YansWifiPhy> ();

    Ptr<DmgAntennaController> staAntCtrl = staMac->GetDmgAntennaController();
    Ptr<DmgAntennaController> apAntCtrl = apMac->GetDmgAntennaController();

    Ptr<MobilityModel> staMob = m_staNodes->Get(staIdx)->
	    GetObject<MobilityModel> ();
    Ptr<MobilityModel> apMob = m_apNodes->Get(apIdx)->
	    GetObject<MobilityModel> ();

    // propagation loss model is the same for AP and STA
    Ptr<PropagationLossModel> loss = staPhy->GetChannel()->
	    GetObject<YansWifiChannel>()->GetPropagationLossModel();
    bool dmgOfdm = staPhy->GetDmgOfdm();

    double azimuthStaAp = CalculateAzimuthAngle(staMob->GetPosition(),
						apMob->GetPosition());
    double elevationStaAp = CalculateElevationAngle(staMob->GetPosition(),
					        apMob->GetPosition());

    //Force antenna point
    staAntCtrl->PointAntenna(apPhy);
    apAntCtrl->PointAntenna(staPhy);

    double staRxPower = loss->CalcRxPower(apPhy->GetTxPowerStart() +
	    apPhy->GetTxGain(), apMob, staMob) +
	    apAntCtrl->GetTxGainDbi(azimuthStaAp + M_PI, -elevationStaAp) +
	    staAntCtrl->GetRxGainDbi(azimuthStaAp, elevationStaAp) +
	    staPhy->GetRxGain();
    double apRxPower = loss->CalcRxPower(staPhy->GetTxPowerStart() +
	    staPhy->GetTxGain(), staMob, apMob) +
	    staAntCtrl->GetTxGainDbi(azimuthStaAp, elevationStaAp) +
	    apAntCtrl->GetRxGainDbi(azimuthStaAp + M_PI, -elevationStaAp) +
	    apPhy->GetRxGain();

    WifiMode apWifiMode = GetWifiMode(staRxPower, dmgOfdm);
    WifiMode staWifiMode = GetWifiMode(apRxPower, dmgOfdm);
    Mac48Address apMacAddr = apMac->GetAddress();
    Mac48Address staMacAddr = staMac->GetAddress();

    staManager->AddDestinationWifiMode(apMacAddr, staWifiMode);
    apManager->AddDestinationWifiMode(staMacAddr, apWifiMode);
  }
}

void
DmgAlmightyController::ConfigureStadardBasedAssociation (void)
{
  for (uint32_t staIdx = 0; staIdx < m_staNodes->GetN(); staIdx++) {
    int selectedAp = -1;
    double selectedApRxPower = 0;

    // Look for the AP with the highest SNR
    for (uint32_t apIdx = 0; apIdx < m_apNodes->GetN(); apIdx++) {

      Ptr<DmgWifiMac> staMac =
	    m_staNodes->Get(staIdx)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetMac()->GetObject<DmgWifiMac>();
      Ptr<DmgWifiMac> apMac =
	    m_apNodes->Get(apIdx)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetMac()->GetObject<DmgWifiMac>();


      Ptr<YansWifiPhy> staPhy =
	    m_staNodes->Get(staIdx)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetPhy()->GetObject<YansWifiPhy> ();
      Ptr<YansWifiPhy> apPhy =
	    m_apNodes->Get(apIdx)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetPhy()->GetObject<YansWifiPhy> ();

      Ptr<DmgAntennaController> staAntCtrl = staMac->GetDmgAntennaController();
      Ptr<DmgAntennaController> apAntCtrl = apMac->GetDmgAntennaController();

      Ptr<MobilityModel> staMob = m_staNodes->Get(staIdx)->
	    GetObject<MobilityModel> ();
      Ptr<MobilityModel> apMob = m_apNodes->Get(apIdx)->
	    GetObject<MobilityModel> ();

      // propagation loss model is the same for AP and STA
      Ptr<PropagationLossModel> loss = staPhy->GetChannel()->
	    GetObject<YansWifiChannel>()->GetPropagationLossModel();
      bool dmgOfdm = staPhy->GetDmgOfdm();

      double azimuthStaAp = CalculateAzimuthAngle(staMob->GetPosition(),
						apMob->GetPosition());
      double elevationStaAp = CalculateElevationAngle(staMob->GetPosition(),
					        apMob->GetPosition());

      //Force antenna point
      staAntCtrl->PointAntenna(apPhy);
      apAntCtrl->PointAntenna(staPhy);

      double staRxPower = loss->CalcRxPower(apPhy->GetTxPowerStart() +
	    apPhy->GetTxGain(), apMob, staMob) +
	    apAntCtrl->GetTxGainDbi(azimuthStaAp + M_PI, -elevationStaAp) +
	    staAntCtrl->GetRxGainDbi(azimuthStaAp, elevationStaAp) +
	    staPhy->GetRxGain();
      double apRxPower = loss->CalcRxPower(staPhy->GetTxPowerStart() +
	    staPhy->GetTxGain(), staMob, apMob) +
	    staAntCtrl->GetTxGainDbi(azimuthStaAp, elevationStaAp) +
	    apAntCtrl->GetRxGainDbi(azimuthStaAp + M_PI, -elevationStaAp) +
	    apPhy->GetRxGain();

      if (selectedAp == -1) {
	selectedAp = apIdx;
	selectedApRxPower = apRxPower;
      } else {
        if (apRxPower > selectedApRxPower) {
	  selectedAp = apIdx;
	  selectedApRxPower = apRxPower;
	}
      }

    }

    m_assocPairs.push_back(selectedAp);
  }

}

void
DmgAlmightyController::AssignEqualAirTime(void)
{
  std::vector<uint32_t> staPerAp;

  for (uint32_t apIdx = 0; apIdx < m_apNodes->GetN(); apIdx++) {
    uint32_t staN = 0;

    for (uint32_t staIdx = 0; staIdx < m_staNodes->GetN(); staIdx++) {
      if (m_assocPairs.at(staIdx) == apIdx) {
	staN++;
      }
    }

    staPerAp.push_back(staN);
  }

  // To each station asssociated with AP apIdx assign the same amount of
  // air-time
  for (uint32_t staIdx = 0; staIdx < m_staNodes->GetN(); staIdx++) {
    m_spAlloc.push_back(1.0/staPerAp.at(m_assocPairs.at(staIdx)));
  }
}

void
DmgAlmightyController::CreateBlockAckAgreement(void)
{
  for (uint32_t staIdx = 0; staIdx < m_staNodes->GetN(); staIdx++) {

    Ptr<DmgWifiMac> staMac =
	    m_staNodes->Get(staIdx)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetMac()->GetObject<DmgWifiMac>();

    PointerValue ptrStaEdca;
    staMac->GetAttribute ("BE_EdcaTxopN", ptrStaEdca);
    Ptr<EdcaTxopN> staEdca = ptrStaEdca.Get<EdcaTxopN> ();

    for (uint32_t apIdx = 0; apIdx < m_apNodes->GetN(); apIdx++) {

      Ptr<DmgWifiMac> apMac =
	    m_apNodes->Get(apIdx)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetMac()->GetObject<DmgWifiMac>();

      PointerValue ptrApEdca;
      apMac->GetAttribute ("BE_EdcaTxopN", ptrApEdca);
      Ptr<EdcaTxopN> apEdca = ptrApEdca.Get<EdcaTxopN> ();

      /* We assume immediate block ack with 0 timeout */
      apEdca->ForceOriginatorBlockAckAgreement(staMac->GetAddress(), AC_BE,
                 0, true);
      staEdca->ForceOriginatorBlockAckAgreement(apMac->GetAddress(), AC_BE,
                 0, true);
    }
  }
}

void
DmgAlmightyController::PrintIdealRxPowerAndMcs(void)
{
  for (uint32_t staIdx = 0; staIdx < m_staNodes->GetN(); staIdx++) {
    for (uint32_t apIdx = 0; apIdx < m_apNodes->GetN(); apIdx++) {

      Ptr<DmgWifiMac> staMac =
	    m_staNodes->Get(staIdx)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetMac()->GetObject<DmgWifiMac>();
      Ptr<DmgWifiMac> apMac =
	    m_apNodes->Get(apIdx)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetMac()->GetObject<DmgWifiMac>();

      Ptr<DmgDestinationFixedWifiManager> staManager = staMac->
	    GetWifiRemoteStationManager()->GetObject<DmgDestinationFixedWifiManager>();
      Ptr<DmgDestinationFixedWifiManager> apManager = apMac->
	    GetWifiRemoteStationManager()->GetObject<DmgDestinationFixedWifiManager>();

      Ptr<YansWifiPhy> staPhy =
	    m_staNodes->Get(staIdx)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetPhy()->GetObject<YansWifiPhy> ();
      Ptr<YansWifiPhy> apPhy =
	    m_apNodes->Get(apIdx)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetPhy()->GetObject<YansWifiPhy> ();

      Ptr<DmgAntennaController> staAntCtrl = staMac->GetDmgAntennaController();
      Ptr<DmgAntennaController> apAntCtrl = apMac->GetDmgAntennaController();

      Ptr<MobilityModel> staMob = m_staNodes->Get(staIdx)->
	    GetObject<MobilityModel> ();
      Ptr<MobilityModel> apMob = m_apNodes->Get(apIdx)->
	    GetObject<MobilityModel> ();

      // propagation loss model is the same for AP and STA
      Ptr<PropagationLossModel> loss = staPhy->GetChannel()->
	    GetObject<YansWifiChannel>()->GetPropagationLossModel();
      bool dmgOfdm = staPhy->GetDmgOfdm();

      double azimuthStaAp = CalculateAzimuthAngle(staMob->GetPosition(),
						apMob->GetPosition());
      double elevationStaAp = CalculateElevationAngle(staMob->GetPosition(),
					        apMob->GetPosition());

      //Force antenna point
      staAntCtrl->PointAntenna(apPhy);
      apAntCtrl->PointAntenna(staPhy);

      double staRxPower = loss->CalcRxPower(apPhy->GetTxPowerStart() +
	    apPhy->GetTxGain(), apMob, staMob) +
	    apAntCtrl->GetTxGainDbi(azimuthStaAp + M_PI, -elevationStaAp) +
	    staAntCtrl->GetRxGainDbi(azimuthStaAp, elevationStaAp) +
	    staPhy->GetRxGain();
      double apRxPower = loss->CalcRxPower(staPhy->GetTxPowerStart() +
	    staPhy->GetTxGain(), staMob, apMob) +
	    staAntCtrl->GetTxGainDbi(azimuthStaAp, elevationStaAp) +
	    apAntCtrl->GetRxGainDbi(azimuthStaAp + M_PI, -elevationStaAp) +
	    apPhy->GetRxGain();

      WifiMode apWifiMode = GetWifiMode(staRxPower, dmgOfdm);
      WifiMode staWifiMode = GetWifiMode(apRxPower, dmgOfdm);



      std::cout << "STA " << staIdx << " - AP " << apIdx << std::endl;
      std::cout << "AP rx power: " << apRxPower << " STA MCS: " <<
	   staWifiMode.GetUniqueName() << std::endl;
      std::cout << "STA rx power: " << staRxPower << " AP MCS " <<
	   apWifiMode.GetUniqueName() << std::endl;
      std::cout << std::endl;
    }
  }

}

void
DmgAlmightyController::PrintAssociationsInfo(void)
{
  for (uint32_t staIdx = 0; staIdx < m_staNodes->GetN(); staIdx++) {
    std::cout << "STA " << staIdx << " associated to AP " <<
	    m_assocPairs.at(staIdx) << std::endl;
  }
}

void
DmgAlmightyController::PrintSpInfo(void)
{
  for (uint32_t staIdx = 0; staIdx < m_staNodes->GetN(); staIdx++) {
    Ptr<DmgBeaconInterval> dmgBiSta = m_staNodes->Get(staIdx)->GetDevice(0)->
	      GetObject<WifiNetDevice>()->GetMac()->GetObject<DmgWifiMac>()->
	      GetDmgBeaconInterval();
    std::vector<Ptr<DmgServicePeriod> > sp = dmgBiSta->GetSps();

    Time spStart = sp.at(0)->GetSpStart();
    Time spStop = sp.at(0)->GetSpStop();

    std::cout << "STA " << staIdx << " SP duration: " << spStop - spStart <<
	    " (start at " << spStart << ", end at " << spStop << ")" << std::endl;

  }
}

} // namespace ns3

