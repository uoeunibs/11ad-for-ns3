/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015 NICOLO FACCHI
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Nicolo' Facchi <nicolo.facchi@gmail.com>
 */
#ifndef DMG_ALMIGHTY_CONTROLLER_H
#define DMG_ALMIGHTY_CONTROLLER_H

#include "ns3/node-container.h"
#include "ns3/object.h"
#include "ns3/wifi-mode.h"
#include <vector>


namespace ns3 {

/* Controller of the DMG network.
 * The controller knows every node (AP and STA) of the network.
 * It is able to compute the standard based association (AP with the highest
 * SNR).
 * It is able to configure the DmgBeaconInterval and the DmgServicePeriod of the
 * nodes.
 * Currently the controller supports only static networks (nodes positions and
 * configurations do not change during the simulations. The nodes are configured
 * at the beginning of the simulations.)
 * In future implementations the controller will be able to handle dynamic
 * networks
 *
 * We suppose that all the nodes are configured in the same way wrt:
 * - Antenna type.
 * - Antenna gain.
 * - Tx Rx gain.
 * - Tx power. 
 * - BI duration.
 * - BI overhead.
 * - rx noise figure.
 */
class DmgAlmightyController : public Object
{
public:
  DmgAlmightyController ();

  virtual ~DmgAlmightyController ();

  /* Set the AP nodes container */
  void SetApNodes (NodeContainer *aps);
  /* Return the AP nodes container */
  NodeContainer *GetApNodes (void);

  /* Set the STA nodes container */
  void SetStaNodes (NodeContainer *stas);
  /* Return the STA nodes container */
  NodeContainer *GetStaNodes (void);

  /* Set the association pairs vector */
  void SetAssocPairs (std::vector<uint32_t> pairs);
  /* Return the association pairs vector */
  std::vector<uint32_t> GetAssocPairs (void);

  /* Set the service period allocation vector */
  void SetServicePeriodAllocation (std::vector<double> spAlloc);
  /* Return the service period allocation vector */
  std::vector<double> GetServicePeriodAllocation (void);

  /* In the current implementation DmgApWifiMac and DmgStaWifiMac do not
   * implement any association procedure. They work as ad-hoc stations and 
   * association is "simulated" at the application level: application level
   * server must be configured on the AP a given statin S is supposed to be
   * associated with. The application level client must be configured on S.
   * Finally the client-server pair must be configured to communicate to each
   * other.
   * For now this is a dummy function. In future implementations it will be used
   * to enforce associations without running the actual frame exchange.
   */
  void EnforceAssociations (void);

  /* The one-hop routing is enforced based on assocPairs.
   * In the current implemantation this is a dummy function because we assume
   * one-hop transmissions between STAs and APs.
   * The one-hop routing is enforced at the application level.
   */
  void EnforceRouting (void);

  /* Setter and Getter for various nodes paramenters.
   * Not all these parameters are currently used.
   */
  void SetAntennaSectorsN (double sectorsN);
  double GetAntennaSectorsN (void);
  void SetAntennaBeamwidth (double beamwidth);
  double GetAntennaBeamwidth (void);
  void SetAntennaGainTxRx (double gain);
  double GetAntennaGainTxRx (void);
  void SetEnergyDetectionThreshold (double detTh);
  double GetEnergyDetectionThreshold (void);
  void SetCcaMode1Threshold (double cca);
  double GetCcaMode1Threshold (void);
  void SetTxGain (double gain);
  double GetTxGain (void);
  void SetRxGain (double gain);
  double GetRxGain (void);
  void SetTxPowerLevels (double txLevels);
  double GetTxPowerLevels (void);
  void SetTxPowerEnd (double txPowerEnd);
  double GetTxPowerEnd (void);
  void SetTxPowerStart (double txPowerStart);
  double GetTxPowerStart (void);
  void SetRxNoiseFigure (double noiseFigure);
  double GetRxNoiseFigure (void);
  void SetBiDuration (uint64_t biDur);
  uint64_t GetBiDuration (void);
  void SetBiOverheadFraction (double biOverhead);
  double GetBiOverheadFraction (void);

  /* Configure a DmgBeaconInterval for each node of the network.
   * A DmgBeaconInterval holds a list of DmgServicePeriod
   */
  void ConfigureBeaconIntervals (void);

  /* Configure the WifiRemoteStationManager (DmgDestinationFixedWifiManager)
   * For each node, the corresponding DmgDestinationFixedWifiManager knows the 
   * ideal transmission MCS for each possible destination.
   */
  void ConfigureWifiManager (void);

  /* Compute the standard-based association.
   * For each STA the controller choose the AP with the highest SNR
   */
  void ConfigureStadardBasedAssociation (void);

  /* For each AP, the controller assign the same amount of air-time to each
   * stations associated to that AP 
   */
  void AssignEqualAirTime(void);

  /* Utility functions for printing informations about the network */
  void PrintIdealRxPowerAndMcs(void);
  void PrintAssociationsInfo(void);
  void PrintSpInfo(void);

  /* Force Block Ack agreement creation */
  void CreateBlockAckAgreement(void);

private:

  /* Function used by ConfigureWifiManager for configuring the 
   * DmgDestinationFixedWifiManager
   */
  WifiMode GetWifiMode (double rxPowerDbi, bool ofdm);

  /* it a caller responsibility to ensure that the internal state
   * of the controller (e.g. assocPairs) is set correctly after one of the following two
   * containers has been modified.
   */
  NodeContainer *m_apNodes; //APs under control
  NodeContainer *m_staNodes; //STAs under control

  /* assocPairs.at(i) = j
   * i is the index of staNodes
   * j is the index of apNodes
   * STA i is associated to AP j
   */
  std::vector<uint32_t> m_assocPairs;

  /* NOTE: Ignore the following three variables. They are not currently used.
   * In the current version we suppose every node is equipped with the same
   * antenna (everyone has the same number of sector and m_antennaSectorN *
   * m_antennaBeamwidth = 360 degress). When one of the following three
   * variables is set the other two are computed according to the value of the
   * first one.
   * NOTE: the real nodes setting in performed by DmgScenarioHelper. The
   * controller keeps this values only for caching purposes.
   * NOTE: if you want to copute the association and resource allocation
   * solution ofline with matlab you must be sure that th configuration file
   * given as input to matlab share the same login to set the RX and TX gain.
   */
  double m_antennaSectorsN;
  double m_antennaBeamwidth;
  double m_antennaGainTxRx;

  /* we assume that the following values are shared by all the nodes and that
   * are the same used in Matlab for computing the solutions of the optimisation
   * problem
   * NOTE: This values are not currently used by the controller.
   */
  double m_energyDetectionThreshold;
  double m_ccaMode1Threshold;
  double m_txGain;
  double m_rxGain;
  double m_txPowerLevels;
  double m_txPowerEnd;
  double m_txPowerStart;
  double m_rxNoiseFigure;

  /* Duration in nanoseconds of the Beacon Interval.
   * We assume that all the APs (and STAs) share this value.
   */
  uint64_t m_biDuration;
  /* Fraction of the Beacon Interval used as overhead (nodes does not transmit
   * during this time).
   * We suppose that this value is the same for all the APs (and STAs)
   */
  double m_biOverhaedFraction; //fraction of the BI duration reserved for overhead

  /* service period allocation
   * spAlloc.at(i) = f
   * staNode i has a fraction f of the available time in the Beacon Interval
   * (m_biDuration - m_biOverheadFraction * m_biDuration)
   */
  std::vector<double> m_spAlloc;
};

} // namespace ns3

#endif /* DMG_ALMIGHTY_CONTROLLER_H */
