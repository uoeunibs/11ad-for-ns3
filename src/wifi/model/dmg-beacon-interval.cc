/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015 NICOLO' FACCHI
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Nicolo' Facchi <nicolo.facchi@gmail.com>
 */
#include "dmg-beacon-interval.h"
#include "ns3/simulator.h"

namespace ns3 {

DmgServicePeriod::DmgServicePeriod ()
{
}

DmgServicePeriod::~DmgServicePeriod ()
{
}

void
DmgServicePeriod::SetSpStart (Time start)
{
  m_spStart = start;
}

Time
DmgServicePeriod::GetSpStart (void)
{
  return m_spStart;
}

void
DmgServicePeriod::SetSpStop (Time stop)
{
  m_spStop = stop;
}

Time
DmgServicePeriod::GetSpStop (void)
{
  return m_spStop;
}

void
DmgServicePeriod::SetSpDestination (Mac48Address dest)
{
  m_spDestination = dest;
}

Mac48Address
DmgServicePeriod::GetSpDestination (void)
{
  return m_spDestination;
}

void
DmgServicePeriod::SetSpDestinationMobility(Ptr<MobilityModel> mob)
{
  m_spDestinationMobility = mob;
}

Ptr<MobilityModel>
DmgServicePeriod::GetSpDestinationMobility (void)
{
  return m_spDestinationMobility;
}

DmgBeaconInterval::DmgBeaconInterval ()
{
  m_dmgAntennaController = 0;
  m_alignAntennaTime = NanoSeconds(0);
  m_initialized = false;
}

DmgBeaconInterval::~DmgBeaconInterval ()
{
}

void
DmgBeaconInterval::SetBiDuration (Time biDur)
{
  m_biDuration = biDur;
}

Time
DmgBeaconInterval::GetBiDuration (void)
{
  return m_biDuration;
}

void
DmgBeaconInterval::AddSp (Time start, Time stop,
			  Mac48Address dest, Ptr<MobilityModel> mob)
{
  Ptr<DmgServicePeriod> sp = CreateObject<DmgServicePeriod> ();
  sp->SetSpStart(start);
  sp->SetSpStop(stop);
  sp->SetSpDestination(dest);
  sp->SetSpDestinationMobility(mob);

  m_sp.push_back(sp);
}

void
DmgBeaconInterval::EraseSp ()
{
  m_sp.clear();
}

Time
DmgBeaconInterval::GetNextSpStart (void)
{
  Time t;

  NS_ASSERT(m_sp.size() > 0);

  Time now = Simulator::Now();
  Time lastBiStart = (now / m_biDuration) *
				 m_biDuration;

  bool nextSpFound = false;
  do {
    for (uint32_t i = 0; i < m_sp.size(); i++) {
      Time spStop = lastBiStart + m_sp.at(i)->GetSpStop();

      if (now < spStop) {
        t = lastBiStart + m_sp.at(i)->GetSpStart();
	nextSpFound = true;
        break;
      }
    }

    lastBiStart += m_biDuration;
  } while (!nextSpFound);

  return t;
}

Time
DmgBeaconInterval::GetNextSpStop (void)
{
  Time t;

  NS_ASSERT(m_sp.size() > 0);

  Time now = Simulator::Now();
  Time lastBiStart = (now / m_biDuration) *
				 m_biDuration;

  bool nextSpFound = false;
  do {
    for (uint32_t i = 0; i < m_sp.size(); i++) {
      Time spStop = lastBiStart + m_sp.at(i)->GetSpStop();

      if (now < spStop) {
        t = spStop;
	nextSpFound = true;
        break;
      }
    }

    lastBiStart += m_biDuration;
  }while (!nextSpFound);

  return t;
}

Mac48Address
DmgBeaconInterval::GetNextSpDestination (void)
{
  Mac48Address mac;

  NS_ASSERT(m_sp.size() > 0);

  Time now = Simulator::Now();
  Time lastBiStart = (now / m_biDuration) *
				 m_biDuration;

  bool nextSpFound = false;
  do {
    for (uint32_t i = 0; i < m_sp.size(); i++) {
      Time spStop = lastBiStart + m_sp.at(i)->GetSpStop();

      if (now < spStop) {
        mac = m_sp.at(i)->GetSpDestination();
	nextSpFound = true;
        break;
      }
    }

    lastBiStart += m_biDuration;
  }while (!nextSpFound);

  return mac;
}

Ptr<MobilityModel>
DmgBeaconInterval::GetNextSpDestinationMobility (void)
{
  Ptr<MobilityModel> mob;

  NS_ASSERT(m_sp.size() > 0);

  Time now = Simulator::Now();
  Time lastBiStart = (now / m_biDuration) *
				 m_biDuration;

  bool nextSpFound = false;
  do {
    for (uint32_t i = 0; i < m_sp.size(); i++) {
      Time spStop = lastBiStart + m_sp.at(i)->GetSpStop();

      if (now < spStop) {
        mob = m_sp.at(i)->GetSpDestinationMobility();
	nextSpFound = true;
        break;
      }
    }

    lastBiStart += m_biDuration;
  }while (!nextSpFound);

  return mob;
}

void
DmgBeaconInterval::SetDmgAntennaController (Ptr<DmgAntennaController> dmgBi)
{
  m_dmgAntennaController = dmgBi;
}

Ptr<DmgAntennaController>
DmgBeaconInterval::GetDmgAntennaController (void)
{
  return m_dmgAntennaController;
}

void
DmgBeaconInterval::AlignAntenna()
{
  Vector targetPos = m_sp.at(m_lastAlignAntennaSpIndex)->
	  GetSpDestinationMobility()->GetPosition();

  m_dmgAntennaController->PointAntenna(targetPos);

  m_lastAlignAntennaSpIndex++;

  if (m_lastAlignAntennaSpIndex == m_sp.size()) {
    m_lastAlignAntennaSpIndex = 0;

    Time now = Simulator::Now();
    Time nextBiStart = ((now / m_biDuration) + 1) *
				 m_biDuration;

    Simulator::Schedule (nextBiStart - now,
			&DmgBeaconInterval::ScheduleNextAntennaAlignment, this);
  } else {
    ScheduleNextAntennaAlignment();
  }
}

void
DmgBeaconInterval::ScheduleNextAntennaAlignment ()
{
  if (m_sp.size() == 0) {
    return;
  }

  if (!m_initialized) {//This happens only at the first call
    m_lastAlignAntennaSpIndex = 0;
    m_initialized = true;
  }

  Time now = Simulator::Now();
  Time lastBiStart = (now / m_biDuration) *
				 m_biDuration;
  m_alignAntennaTime = lastBiStart + m_sp.at(m_lastAlignAntennaSpIndex)->
	  GetSpStart();

  m_alignAntenna = Simulator::Schedule (m_alignAntennaTime - now,
					 &DmgBeaconInterval::AlignAntenna, this);

}

std::vector<Ptr<DmgServicePeriod> >
DmgBeaconInterval::GetSps(void)
{
  return m_sp;
}

} // namespace ns3

