#include <iomanip>
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/internet-module.h"

using namespace ns3;

#define SSID_STR		"test"
#define DESTINATION_PORT_BASE	20000
#define MIN_WIFI_MAC_QUEUE_SIZE 800

#define TP_FILENAME		"tp"

struct sim_config {
  /* Number of client nodes */
  uint32_t staNodesN;

  /* NUmber of AP nodes */
  uint32_t apNodesN;

  /* Client nodes cointainer */
  NodeContainer *staNodes;

  /* AP nodes container */
  NodeContainer *apNodes;

  /* Pointer to the Oracle controller (DmgAlmightyController)
   * The controller knows every node in the network
   */
  Ptr<DmgAlmightyController> dmgCtrl;

  /* Simulation duration 
   * Nodes transmit data from second 1.0 to second simuationTime + 1.0
   */
  double simulationTime;

  /* Transmission rate at the application level in Mb/s */
  double udpTxRate;
  bool randomUdpTxRates;
  double *staUdpTxRates;

  /* UDP payload bytes */
  uint32_t udpPayloadBytes;

  /* if true pcap capture files will be generated in the working directory */
  bool enablePcap;

  /* All the nodes will be placed randomly in a square
   * square_side_l x square_side_l (meters)
   */
  double square_side_l;

  /* List of UDP servers.
   * Used for reporting the througput
   */
  ApplicationContainer *servers;

  /* support state variable for throughput report generation */
  Time *lastReportTime;
  double *lastRxMbits;

  /* Minimum frame Bit Error Rate (BER).
   * Remember that a complete frame exchange is composed by a data
   * frame + ack frame. The minimum BER is applied to both the frames. If for a
   * given station you want a FLR l then:
   * frameMinBer  = 1 - sqrt(1 - l, bits_ack + bits_data)
   */
  double frameMinBer;

  /* Antenna gain in dBi.
   * This value is the same for all the nodes and is applied both
   * during reception and during transmission
   */
  double txRxAntennaGainDbi;

  /* Reception and transmission gain in dBi.
   * The gain is the same for all the nodes
   */
  double txRxGainDbi;

  /* Transmission power in dBi.
   * All the nodes use the same transmission power.
   */ 
  double txPowerDbi;

  /* Beacon Interval (BI) duration in ns */
  uint64_t biDurationNs;

  /* Fraction of the BI reserved for protocol overhead.
   * This is the same for all the APs
   */
  double biOverheadFraction;

  /* true for using DMG OFDM PHY.
   * false for using SC OFDM PHY.
   * MCS0 (CTRL) is always enabled
   */
  bool dmgOfdm;

  /* Max number of aggregated MPDU */
  uint32_t nMpdus;

  /* 0 for uplink only, 1 for downlink only, 2 for mixed */
  uint32_t flowDir;

  /* Configure WifiMacQueue size in order to match the application level rate.
   * (Useful for non-saturation scenario)
   */
  bool nonSatQueues;
};

struct sim_config config;

/* Utility function used to print the simulation configuration */
void printSimInfo ()
{
  std::cout << "Simulating " << config.apNodesN << " APs and " << config.staNodesN
	  << " clients" << std::endl;

  std::cout << std::endl;

  std::cout << "*********************************" << std::endl;
  std::cout << "NODES POSITIONS (x, y)" << std::endl;
  std::cout << "*********************************" << std::endl;
  std::cout << "Nodes randomly placed in a square " <<
	  config.square_side_l << "x" << config.square_side_l << " (m)" <<
	  std::endl;
  for (uint32_t i = 0; i < config.apNodesN; i++) {
    Vector pos =
	    config.apNodes->Get(i)->GetObject<MobilityModel>()->GetPosition();
    std::cout << "AP " << i << " (" << pos.x << ", " << pos.y << ")" <<
	    std::endl;
  }
  for (uint32_t i = 0; i < config.staNodesN; i++) {
    Vector pos =
	    config.staNodes->Get(i)->GetObject<MobilityModel>()->GetPosition();
    std::cout << "STA " << i << " (" << pos.x << ", " << pos.y << "), ";

    if (config.randomUdpTxRates) {
      std::cout << "Rate: " << *(config.staUdpTxRates + i) << "Mb/s" << std::endl;
    } else {
      std::cout << "Rate: " << config.udpTxRate << "Mb/s" << std::endl;
    }
  }
  std::cout << std::endl;

  std::cout << "*********************************" << std::endl;
  std::cout << "SIMULATION CONFIGURATION" << std::endl;
  std::cout << "*********************************" << std::endl;

  std::cout << "Simulation time: " << config.simulationTime << std::endl;

  std::cout << "11ad mode: " << (config.dmgOfdm ? "OFDM" : "SC") << std::endl;

  std::cout << "Traffic flow: " << config.flowDir << std::endl;

  std::cout << "Non-saturation queues: " << config.nonSatQueues << std::endl;

  std::cout << "UDP payload " << config.udpPayloadBytes <<
	  " bytes" << std::endl;

  std::cout << "Application level data rate: " << config.udpTxRate << "Mbps" <<
	  std::endl;

  std::cout << "PCAP capture enabled: " << (config.enablePcap ? "true" : "false")
	  << std::endl;

  std::cout << "Min Frame BER: " << config.frameMinBer << std::endl;

  std::cout << "Transmission power: " << config.txPowerDbi << "dBi" <<
	  std::endl;

  std::cout << "Transmission and Reception gain: " << config.txRxGainDbi << "dBi" <<
	  std::endl;

  std::cout << "TX and RX antenna gain: " << config.txRxAntennaGainDbi << "dBi"
	  << std::endl;

  std::cout << "Antenna beamwidth degrees: " <<
	  config.apNodes->Get(0)->GetDevice(0)->GetObject<WifiNetDevice>()->
	  GetMac()->GetObject<DmgWifiMac>()->GetDmgAntennaController()->
	  GetBeamwidthDegrees() << std::endl;

  std::cout << "BI duration: " << config.biDurationNs << "ns" << std::endl;

  std::cout << "BI overhead: " << (uint64_t)(config.biOverheadFraction *
	  config.biDurationNs) << "ns" << std::endl;

  if (config.nMpdus > 0) {
    std::cout << "Aggregation enabled: A-MPDU max size -> " << config.nMpdus <<
      " MPDUs" << std::endl;
  } else {
    std::cout << "Aggregation disabled" << std::endl;
  }

  std::cout << std::endl;

  std::cout << "*********************************" << std::endl;
  std::cout << "IDEAL RX POWER and MCS" << std::endl;
  std::cout << "*********************************" << std::endl;
  config.dmgCtrl->PrintIdealRxPowerAndMcs();
  std::cout << std::endl;

  std::cout << "*********************************" << std::endl;
  std::cout << "ASSOCIATIONS (STANDARD BASED)" << std::endl;
  std::cout << "*********************************" << std::endl;
  config.dmgCtrl->PrintAssociationsInfo();
  std::cout << std::endl;

  std::cout << "*********************************" << std::endl;
  std::cout << "SERVICE PERIODS" << std::endl;
  std::cout << "*********************************" << std::endl;
  config.dmgCtrl->PrintSpInfo();
  std::cout << std::endl;
}

/* Utiity function used to generate the throughput (+ successes and failures)
 * report. This function is called every biDurationNs ns.
 */
void PrintProgress(struct sim_config *config,
		   Ptr<OutputStreamWrapper> stream_throughput)
{
  Time now_t = Simulator::Now ();

  /* The first time the function is called we schedule the next call after
   * biDurationNs ns and then return
   */
  if (*(config->lastReportTime) == MicroSeconds(0)) {
	  *(config->lastReportTime) = now_t;
	  Simulator::Schedule(NanoSeconds(config->biDurationNs), PrintProgress, config,
			      stream_throughput);
	  return;
  }

  std::cout << now_t << std::endl;

  /* print the simulation time in the first column of the report file */
  *stream_throughput->GetStream() << now_t;
  *stream_throughput->GetStream() << std::fixed << std::setprecision(6);


  /* For each client node print 3 columns in the report file: througputh (Mb/s),
   * successes and failures */
  std::vector<uint32_t> assocPairs = config->dmgCtrl->GetAssocPairs();
  for (uint32_t i = 0; i < config->staNodesN; i++)
  {
    uint64_t rx_packets;
    double cur_rx_Mbits;
    uint32_t apIdx = assocPairs.at(i);
    Mac48Address macDest;
    Ptr<EdcaTxopN> edca;

    if (config->flowDir == 0) {
      macDest = config->apNodes->Get(apIdx)->GetDevice(0)->GetObject<WifiNetDevice>()->
        GetMac()->GetObject<DmgWifiMac>()->GetAddress();

      edca = config->staNodes->Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->GetMac()->
		    GetObject<DmgStaWifiMac> ()->GetBEQueue ();
    } else if (config->flowDir == 1) {
      macDest = config->staNodes->Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->
        GetMac()->GetObject<DmgWifiMac>()->GetAddress();

      edca = config->apNodes->Get(apIdx)->GetDevice(0)->GetObject<WifiNetDevice>()->GetMac()->
		    GetObject<DmgApWifiMac> ()->GetBEQueue ();
    }


    rx_packets = DynamicCast<UdpServer>(config->servers->Get (i))->
	  GetReceived ();
    cur_rx_Mbits = (rx_packets * config->udpPayloadBytes * 8.0) / 1e6;

    *stream_throughput->GetStream() << " " <<
	    (cur_rx_Mbits - config->lastRxMbits[i]) /
	    ((now_t - *(config->lastReportTime)).GetNanoSeconds() / 1e9) <<
	    " " << edca->GetSuccesses(macDest) << " " << edca->GetFailures(macDest);

    config->lastRxMbits[i] = cur_rx_Mbits;
  }
  *stream_throughput->GetStream() << std::endl;


  Simulator::Schedule(NanoSeconds(config->biDurationNs), PrintProgress, config,
			      stream_throughput);

  *(config->lastReportTime) = now_t;

}

/* Nodes configuration finalization:
 * - ARP setup
 * - Minimum frame BER setup
 */
void
FinalizeConfig(struct sim_config *config)
{
  NodeContainer allNodes (*(config->apNodes), *(config->staNodes));

  for (uint32_t i = 0; i < allNodes.GetN(); i++)
  {
    /* This loop is used to force the ARP tables on each node */
    for (uint32_t j = 0; j < allNodes.GetN(); j++)
    {
      if (i == j)
        continue;

      Address mac = allNodes.Get(j)->GetDevice(0)->GetAddress();
      Ipv4Address ip = allNodes.Get(j)->GetObject<Ipv4>()->
	      GetAddress(1,0).GetLocal();
      Ptr<ArpCache> arpCache = allNodes.Get(i)->GetObject<Ipv4L3Protocol>()->
	      GetInterface(1)->GetArpCache();

      if (arpCache == NULL)
        arpCache = CreateObject<ArpCache>();
      arpCache->SetAliveTimeout(Seconds (config->simulationTime + 1));
      ArpCache::Entry *entry = arpCache->Add(ip);
      entry->MarkWaitReply(0);
      entry->MarkAlive(mac);
    }

    /* Configure the minimum frame BER */
    Ptr<YansWifiPhy> yansPhy = allNodes.Get(i)->GetDevice(0)->
	    GetObject<WifiNetDevice>()->GetPhy()->
	    GetObject<YansWifiPhy> ();
    yansPhy->SetFrameMinBer(config->frameMinBer);
  }


}

/* Utility function used to configure the DMG controller */
void SetupDmgController (struct sim_config *config)
{

  /* WARNING: in the current implementation it is suggested to call the
   * functions used to configure the DmgALmightyController as in this example.
   */

  /* We pass the container of the AP nodes to the DmgAlmightyController */
  config->dmgCtrl->SetApNodes(config->apNodes);
  /* We pass the container of the STA nodes to the DmgAlmightyController */
  config->dmgCtrl->SetStaNodes(config->staNodes);
  /* Let the DmgAlmightyController know the Beacon Interval duration */
  config->dmgCtrl->SetBiDuration(config->biDurationNs);
  /* Let the DmgAlmightyController know the fraction of the Beacon Interval that
   * is considered overhead and is not used for data transmission */
  config->dmgCtrl->SetBiOverheadFraction(config->biOverheadFraction);
  /* We ask the DmgAlmightyController to automatically compute the STA-AP
   * associations as defined by the standard (STA select the AP with the highest
   * SNR)
   */
  config->dmgCtrl->ConfigureStadardBasedAssociation();
  /* Associate the STAs to the APs (this is a dummy function in the current
   * implementation. See commenti in dmg-almighty-controller.h)
   */
  config->dmgCtrl->EnforceAssociations();
  /* We ask the DmgAlmightyController to configure the Service Periods on each
   * node. By default the controller assign, for each AP, equal air-time to each
   * associated STA.
   */
  config->dmgCtrl->AssignEqualAirTime();
  /* Configure the DmgBeaconInterval on each node with the list of Secrive
   * Periods.
   */
  config->dmgCtrl->ConfigureBeaconIntervals();
  /* Configure the DmgDestinationFixedWifiManager */
  config->dmgCtrl->ConfigureWifiManager();

  if (config->nMpdus > 0) {
    /* Create the Block Ack Agreements */
    config->dmgCtrl->CreateBlockAckAgreement();
  }
}

/* Utility function used to finalize the configuration of the Nodes in the
 * network */
void SetupDmgNodes (struct sim_config *config)
{
  NodeContainer allNodes (*(config->apNodes), *(config->staNodes));

  for (uint32_t i = 0; i < allNodes.GetN(); i++) {
    /* Currently DMG nodes support only antennas whose type is ConeAntenna */
    Ptr<ConeAntenna> ant = CreateObject<ConeAntenna> ();
    /* Configure the gain (Rx and Tx) of the antenna */
    ant->SetGainDbi(config->txRxAntennaGainDbi);

    /* The antenna is managed by a DmgAntennaController */
    Ptr<DmgAntennaController> antCtrl = CreateObject<DmgAntennaController> ();
    antCtrl->SetAntenna(ant);
    antCtrl->SetPhy(allNodes.Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->
		    GetPhy());

    /* Configure the DmgWifiMac with the DmgAntenna controller just created */
    allNodes.Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetMac()->GetObject<DmgWifiMac>()->SetDmgAntennaController(antCtrl);

    /* Configure the DmgWifiMac with a new DmgBeaconInterval */
    allNodes.Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetMac()->GetObject<DmgWifiMac>()->
	    SetDmgBeaconInterval(CreateObject<DmgBeaconInterval>());

    /* It's important to configure a proper propagation time */
    allNodes.Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetMac()->GetObject<DmgWifiMac>()->SetPropagationGuard(
			Seconds(sqrt(pow(config->square_side_l, 2) * 2) /
				    3e8));

    /* Configure phy level parameters */
    Ptr<YansWifiPhy> yPhy =
	    allNodes.Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->
	    GetPhy()->GetObject<YansWifiPhy>();
    yPhy->SetTxGain(config->txRxGainDbi);
    yPhy->SetRxGain(config->txRxGainDbi);
    yPhy->SetTxPowerStart(config->txPowerDbi);
    yPhy->SetTxPowerEnd(config->txPowerDbi);
    /* Sensitivity model includes implementation loss and noise figure */
    yPhy->SetRxNoiseFigure(0);

  }

  if (config->nonSatQueues) {
    /* Compute the number of Application packet produced per BI */
    uint32_t packetPerBi = std::ceil((config->biDurationNs / 1e9) /
              ((config->udpPayloadBytes * 8.0) / (config->udpTxRate * 1e6)));

    for (uint32_t i = 0; i < config->staNodes->GetN(); i++) {

      if (packetPerBi > MIN_WIFI_MAC_QUEUE_SIZE ) {
        config->staNodes->Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->GetMac()->
	          GetObject<DmgStaWifiMac> ()->GetBEQueue ()->
            SetMaxQueuePacketNumberPerDestination(packetPerBi);
      }
    }

    for (uint32_t i = 0; i < config->apNodes->GetN(); i++) {

      if (packetPerBi > MIN_WIFI_MAC_QUEUE_SIZE ) {
        config->apNodes->Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->GetMac()->
	          GetObject<DmgApWifiMac> ()->GetBEQueue ()->
            SetMaxQueuePacketNumberPerDestination(packetPerBi);
      }
    }

  }
}

/* Utility function used to configure the Application level UDP client and
 * server. 
 * This function is important because the aplication level client'server pair is
 * used to "simulate" the AP-STA association.
 * The client runs on the STA and the server runs on the AP the STA is supposed
 * to be associated with.
 */
void SetupUpdApplicationsUploadTest (struct sim_config *config)
{
  /* The DmgAlmightyController knows the STA-AP association pairs */
  Ptr<UniformRandomVariable> unif_rng = CreateObject<UniformRandomVariable> ();
  std::vector<uint32_t> assocPairs = config->dmgCtrl->GetAssocPairs();

  for (uint32_t i = 0; i < config->staNodes->GetN(); i++) {
    uint32_t apIdx = assocPairs.at(i);

    Ipv4Address ipDestination = config->apNodes->Get(apIdx)->
	    GetObject<Ipv4>()->GetAddress(1,0).GetLocal();

    /* configure the server on the AP */
    UdpServerHelper server (DESTINATION_PORT_BASE + i);
    ApplicationContainer serverApp = server.Install
        (config->apNodes->Get(apIdx));
    serverApp.Start (Seconds (0.0));
    serverApp.Stop (Seconds (config->simulationTime + 1));
    config->servers->Add(serverApp);

    /* Configure the client on the STA */
    UdpClientHelper client (ipDestination, DESTINATION_PORT_BASE + i);
    client.SetAttribute ("MaxPackets", UintegerValue (4294967295u));

    if (config->randomUdpTxRates) {
      *(config->staUdpTxRates + i) = unif_rng->GetValue() *
                                    config->udpTxRate;

      client.SetAttribute ("Interval", TimeValue
                          (ns3::Time::FromDouble ((
                          config->udpPayloadBytes * 8.0) /
                          ((*(config->staUdpTxRates + i)) * 1e6),
                          ns3::Time::S))); //packets/s
    } else {
      client.SetAttribute ("Interval", TimeValue
                          (ns3::Time::FromDouble ((
                          config->udpPayloadBytes * 8.0) /
                          (config->udpTxRate * 1e6),
                          ns3::Time::S))); //packets/s
    }

    client.SetAttribute ("PacketSize", UintegerValue (config->udpPayloadBytes));

    ApplicationContainer clientApp = client.Install (config->staNodes->Get(i));

    /* The real communication starts at 1s */
    clientApp.Start (Seconds (1.0));
    clientApp.Stop (Seconds (config->simulationTime + 1));
  }

}

void SetupUpdApplicationsDownloadTest (struct sim_config *config)
{
  /* The DmgAlmightyController knows the STA-AP association pairs */
  Ptr<UniformRandomVariable> unif_rng = CreateObject<UniformRandomVariable> ();
  std::vector<uint32_t> assocPairs = config->dmgCtrl->GetAssocPairs();

  for (uint32_t i = 0; i < config->staNodes->GetN(); i++) {
    uint32_t apIdx = assocPairs.at(i);

    Ipv4Address ipDestination = config->staNodes->Get(i)->
	    GetObject<Ipv4>()->GetAddress(1,0).GetLocal();

    /* configure the server on the AP */
    UdpServerHelper server (DESTINATION_PORT_BASE + i);
    ApplicationContainer serverApp = server.Install
        (config->staNodes->Get(i));
    serverApp.Start (Seconds (0.0));
    serverApp.Stop (Seconds (config->simulationTime + 1));
    config->servers->Add(serverApp);

    /* Configure the client on the STA */
    UdpClientHelper client (ipDestination, DESTINATION_PORT_BASE + i);
    client.SetAttribute ("MaxPackets", UintegerValue (4294967295u));

    if (config->randomUdpTxRates) {
      *(config->staUdpTxRates + i) = unif_rng->GetValue() *
                                    config->udpTxRate;

      client.SetAttribute ("Interval", TimeValue
                          (ns3::Time::FromDouble ((
                          config->udpPayloadBytes * 8.0) /
                          ((*(config->staUdpTxRates + i)) * 1e6),
                          ns3::Time::S))); //packets/s
    } else {
      client.SetAttribute ("Interval", TimeValue
                          (ns3::Time::FromDouble ((
                          config->udpPayloadBytes * 8.0) /
                          (config->udpTxRate * 1e6),
                          ns3::Time::S))); //packets/s
    }

    client.SetAttribute ("PacketSize", UintegerValue (config->udpPayloadBytes));

    ApplicationContainer clientApp = client.Install (config->apNodes->Get(apIdx));

    /* The real communication starts at 1s */
    clientApp.Start (Seconds (1.0));
    clientApp.Stop (Seconds (config->simulationTime + 1));
  }


}

void SetupUpdApplicationsUploadDownloadTest (struct sim_config *config)
{
  std::cerr << "WARNING: mixed upload and download traffic not supported yet." << std::endl;
}

int main (int argc, char *argv[])
{
  /* By default the simulation lasts for 10 seconds */
  double simulationTime = 10;
  /* By default the application layer TX rate is 54Mb/s */
  double udpTxRate = 54;
  /* By default the simulation seed is the NS-3 default */
  int32_t rngSeed = -1;
  /* By default there are 4 STAs */
  uint32_t staNodesN = 4;
  /* By default there are 2 APs */
  uint32_t apNodesN = 2;
  /* By default pcap tracing is disabled */
  bool enablePcap = false;
  /* By default UDP payload is 1470 bytes */
  uint32_t payloadSize = 1470;
  /* By default nodes are placed in a square area of
   * square_dise_l x square_side_l meters */
  double square_side_l = 20;
  /* By default frame min BER is 0 (The real values depends on the relative
   * positions of the nodes */
  double frameMinBer = 0;
  /* Operating frequency in Hz */
  double freq = 59.4e9;
  /* By default Tx and Rx antenna gain in 15 dBi */
  double txRxAntennaGainDbi = 15;
  /* By default the Tx and Rx gain is 1 dBi */
  double txRxGainDbi = 1.0;
  /* By default the Tx power is 10 dBi */
  double txPowerDbi = 10;
  /* By default Beacon Interval duration is 102.4ms */
  uint64_t biDurationNs = 1024000 * 100;
  /* By default 10% of the Beacon Interval is considered overhead */
  double biOverheadFraction = 0.1;
  /* By default OFDM phy is used */
  bool dmgOfdm = true;
  /* Max # aggregated MPDU */
  uint32_t nMpdus = 0;
  /* Traffic flow direction. Uplink by default */
  uint32_t flowDir = 0;
  /* Set to true for increasing the queues size in order to match the application data rate */
  bool nonSatQueues = false;
  /* Use a random UDP tx rate for each station (0<=sta_rate<=udpTxRate) */
  bool randomUdpTxRates = false;

  /* File stream for throughput report.
   * By default saved in the current working directory with the name "tp"
   */
  AsciiTraceHelper asciiTraceHelper;
  Ptr<OutputStreamWrapper> tp_stream =
	  asciiTraceHelper.CreateFileStream (TP_FILENAME);


  /******************************
   * Command line args parsing
   *****************************/
  CommandLine cmd;
  cmd.AddValue("udpTxRate", "UDP Transmission rate (Mb/s)", udpTxRate); //number of aggregated MPDUs specified by the user
  cmd.AddValue("payloadSize", "UDP Payload size in bytes", payloadSize);
  cmd.AddValue("simulationTime", "Simulation time in seconds", simulationTime);
  cmd.AddValue("staNodesN", "Number of STA nodes", staNodesN);
  cmd.AddValue("apNodesN", "Number of AP nodes", apNodesN);
  cmd.AddValue("pcap", "true for pcap capture", enablePcap);
  cmd.AddValue("seed", "RNG seed", rngSeed);
  cmd.AddValue("MinBer", "Minimum bit error rate", frameMinBer);
  cmd.AddValue("square",
	       "Nodes are randomly positioned in a square area of square x square meters", square_side_l);
  cmd.AddValue("gainAntennaDbi", "TX and RX antenna gain (dBi)", txRxAntennaGainDbi);
  cmd.AddValue("gainDbi", "TX and RX gain (dBI)", txRxGainDbi);
  cmd.AddValue("txPowerDbi", "Tx power (dBi)", txPowerDbi);
  cmd.AddValue("biDur", "Beacon Interval duration in ns", biDurationNs);
  cmd.AddValue("biOH", "Beacon Interval overhead fraction", biOverheadFraction);
  cmd.AddValue("dmgOfdm", "True to enable OFDM MCSs, false to use SC MCSs", dmgOfdm);
  cmd.AddValue("nMpdus", "Number of aggregated MPDUs", nMpdus);
  cmd.AddValue("flowDir", "Traffic flow direction (0 for uplink, 1 for downlink, 2 for mixed", flowDir);
  cmd.AddValue("nonSatQ", "Set to true for non saturation scenario (default false)", nonSatQueues);
  cmd.AddValue("randUdpTxRate", "Set to true for using a random UDP tx rate (between 0 and udpTxRate) for each station (default false)", randomUdpTxRates);
  cmd.Parse (argc, argv);

  config.staNodesN = staNodesN;
  config.apNodesN = apNodesN;
  config.staNodes = new NodeContainer();
  config.apNodes = new NodeContainer();
  config.dmgCtrl = CreateObject<DmgAlmightyController> ();
  config.udpTxRate = udpTxRate;
  config.udpPayloadBytes = payloadSize;
  config.enablePcap = enablePcap;
  config.simulationTime = simulationTime;
  config.square_side_l = square_side_l;
  config.frameMinBer = frameMinBer;
  config.txRxAntennaGainDbi = txRxAntennaGainDbi;
  config.txPowerDbi = txPowerDbi;
  config.txRxGainDbi = txRxGainDbi;
  config.biDurationNs = biDurationNs;
  config.biOverheadFraction = biOverheadFraction;
  config.flowDir = flowDir;
  config.nonSatQueues = nonSatQueues;
  config.dmgOfdm = dmgOfdm;
  config.randomUdpTxRates = randomUdpTxRates;
  config.staUdpTxRates = (double *) malloc(sizeof(double) * config.staNodesN);

  if (!config.staUdpTxRates) {
    std::cerr << "Error in allocating memory for staUdpTxRates" << std::endl;
  }

  if (nMpdus > 64) {
    nMpdus = 64;
  }
  config.nMpdus = nMpdus;
  config.servers = new ApplicationContainer();
  config.lastReportTime = new Time(MicroSeconds(0));
  config.lastRxMbits = (double *) malloc(sizeof(double) *
					    config.staNodesN);
  memset(config.lastRxMbits, 0,
	 sizeof(double) * config.staNodesN);

  /******************************
   * Set Seed and initialize an Uniform Random Variable
   *****************************/
  if (rngSeed >= 0)
    RngSeedManager::SetSeed (rngSeed);

  Ptr<UniformRandomVariable> unif_rng = CreateObject<UniformRandomVariable> ();

  /******************************
   * Disable RTS/CTS
   *****************************/
  Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold",
			StringValue ("999999"));

  /******************************
   * Disable fragmentation
   *****************************/
  Config::SetDefault ("ns3::WifiRemoteStationManager::FragmentationThreshold",
		      StringValue ("990000"));

  /******************************
   * Create AP and STA nodes
   *****************************/
  config.apNodes->Create(config.apNodesN);
  config.staNodes->Create(config.staNodesN);

  /******************************
   * Yans wifi and channel configuration 
   *****************************/
  YansWifiChannelHelper channelHelper;
  channelHelper.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  /* Friis model with standard-specific wavelength */
  channelHelper.AddPropagationLoss ("ns3::FriisPropagationLossModel",
				    "Frequency", DoubleValue(freq));

  YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
  phy.Set("dmgOfdm", BooleanValue(config.dmgOfdm));
  Ptr<YansWifiChannel> channel = channelHelper.Create();
  phy.SetChannel (channel);
  phy.SetErrorRateModel("ns3::SensitivityModel60GHz");

  /******************************
   * MAC configuration 
   *****************************/
  WifiHelper wifi = WifiHelper::Default ();
  wifi.SetStandard (WIFI_PHY_STANDARD_80211ad);

  wifi.SetRemoteStationManager ("ns3::DmgDestinationFixedWifiManager");

  Ssid ssid = Ssid (SSID_STR);

  DmgWifiMacHelper clientMac = DmgWifiMacHelper::Default ();
  DmgWifiMacHelper apMac = DmgWifiMacHelper::Default ();
  NetDeviceContainer devices;

  clientMac.SetType ("ns3::DmgStaWifiMac");
  apMac.SetType ("ns3::DmgApWifiMac");

  if (config.nMpdus > 0) {
    clientMac.SetBlockAckThresholdForAc (AC_BE, 1);
    apMac.SetBlockAckThresholdForAc (AC_BE, 1);

    clientMac.SetMpduAggregatorForAc (AC_BE,"ns3::MpduStandardAggregator",
        "MaxAmpduSize", UintegerValue (config.nMpdus * (config.udpPayloadBytes + 100)));
    apMac.SetMpduAggregatorForAc (AC_BE,"ns3::MpduStandardAggregator",
        "MaxAmpduSize", UintegerValue (config.nMpdus * (config.udpPayloadBytes + 100)));
  }

  devices = wifi.Install (phy, apMac, *(config.apNodes));
  devices.Add (wifi.Install(phy, clientMac, *(config.staNodes)));

  /******************************
   * Set node positions 
   *****************************/
  MobilityHelper mobility;
  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();

  for (uint32_t i = 0; i < (config.staNodesN + config.apNodesN); i++)
  {
    /* nodes are randomly placed inside a square area of
     * square_side_l x square_side_l meters */
    Vector posNode (unif_rng->GetValue() * config.square_side_l,
                         unif_rng->GetValue() * config.square_side_l,
                         0);

    positionAlloc->Add(posNode);
  }


  mobility.SetPositionAllocator (positionAlloc);
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (*(config.apNodes));
  mobility.Install (*(config.staNodes));


  /******************************
   * Internet stack configuration
   *****************************/
  InternetStackHelper stack;
  stack.Install (*(config.staNodes));
  stack.Install (*(config.apNodes));

  Ipv4AddressHelper address;

  address.SetBase ("192.168.1.0", "255.255.255.0");
  Ipv4InterfaceContainer interfaces = address.Assign (devices);

  /******************************
   * Global routing configuration
   *****************************/
  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

  /******************************
   * Pcap tracing configuration
   *****************************/
  NodeContainer allNodes (*(config.apNodes), *(config.staNodes));
  if (config.enablePcap)
  {
    phy.SetPcapDataLinkType(YansWifiPhyHelper::DLT_IEEE802_11_RADIO); 
    for (uint32_t i = 0; i < allNodes.GetN(); i++)
    {
      phy.EnablePcap("cap", allNodes.Get(i)->GetDevice(0), true);
    }
  }


  /******************************
   * Finalize configuration and start simulation 
   *****************************/
  SetupDmgNodes (&config);
  SetupDmgController(&config);

  if (config.flowDir == 0) {
    SetupUpdApplicationsUploadTest(&config);
  } else if (config.flowDir == 1) {
    SetupUpdApplicationsDownloadTest(&config);
  } else if (config.flowDir == 2) {
    SetupUpdApplicationsUploadDownloadTest(&config);
    return 0;
  } else {
    std::cerr << "ERROR: unsupported traffic flow configuration" << std::endl;
    return 1;
  }

  printSimInfo();

  Simulator::Schedule(Seconds(0.9999999), FinalizeConfig, &config);
  Simulator::Schedule(Seconds(1), PrintProgress, &config,
			      tp_stream);

  Simulator::Stop (Seconds (config.simulationTime + 1));
  Simulator::Run ();
  Simulator::Destroy ();

  return 0;
}
